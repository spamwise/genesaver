// Genesaver: copyright 2003 Sam Stafford.

class DNA;

class Animal :
	public Thing
{
public:
	Animal( DNA* dna );
	~Animal(void);

	virtual void Render();
	void RenderEnergy();
	virtual void See( float d2, Color c, float diet, float angle );
	virtual void Step();

	Animal* prev;
	Animal* next;

	Color color; // animal color

	float angle; // radians
	float diet;  // 0 to 1, 0=herbivore, 1=carnivore

	int pchewing; // number of turns till next time to eat a plant
	int achewing; // number of turns till next time to eat an animal
	int fighting; // number of turns till next time to attack a friend
	int mating;   // did creature just mate?

	float step; // percent of max step to take
	float turn; // percent of max turn to make

	bool dead; //dead?
	bool tagged; //is World->tagged

	Brain brain;
	DNA* dna;
};

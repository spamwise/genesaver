// Genesaver: copyright 2003 Sam Stafford.

#include <GL/glut.h>
#include "globals.h"
#include "Thing.h"

Thing::Thing(void)
{
}

Thing::~Thing(void)
{
}

void Thing::RenderBy( float ax, float ay )
{
	// Wrap around world edge.
	float tx = x;
	float ty = y;
	x = XNearest( ax );
	y = YNearest( ay );

	bool vis = (x - ax)*(x - ax) + (y - ay)*(y - ay) <= A_V*A_V;

	if ( !vis && settings.drawvis )
	{
		x = tx;
		y = ty;
		return;
	}

	if ( !vis ) fsettings.alpha = 0.2;

	glMatrixMode( GL_MODELVIEW );
	
	glPushMatrix();

	glTranslatef( 0, 0.5, 0 ); //move to top of screen
	glScalef( 0.5 / A_V, 0.5 / A_V, 0.5 / A_V ); //scale to vis range
	glTranslatef( -ax, -ay, 0 ); //center on target creature

	Render();

	glPopMatrix();

	x = tx;
	y = ty;
	fsettings.alpha = 1.0;
	return;
}

float Thing::XNearest( float ax )
{
	if ( ax > x )
	{
		if ( ax - x > 1.0 ) return x + 2.0;
		else return x;
	}
	else
	{
		if ( x - ax > 1.0 ) return x - 2.0;
		else return x;
	}
}

float Thing::YNearest( float ay )
{
	if ( ay > y )
	{
		if ( ay - y > 1.0 ) return y + 2.0;
		else return y;
	}
	else
	{
		if ( y - ay > 1.0 ) return y - 2.0;
		else return y;
	}
}

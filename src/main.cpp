// Genesaver: copyright 2003 Sam Stafford.

#ifndef WIN32
//Win32 builds use winmain.cpp instead.
//This version uses GLUT and should run elsewhere,
//but I haven't tried it out to be sure.

#include <stdio.h>
#include <stdlib.h>
#include <GL/glut.h>
#include <time.h>

#include "globals.h"
#include "util.h"
#include "DNA.h"
#include "World.h"
#include "Jungle.h"

int config();
void display();
void timer( int );
void kdone( unsigned char, int, int );
void mdone( int, int );

void loadsettings();

// mouse tracking for mdone()
bool mstart;
int mx, my;

int main( int argc, char** argv )
{
	extern World *world;

	srand( time(NULL ) );
	mstart = false;

	loadsettings();

	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_DOUBLE | ( settings.AA ? GLUT_ALPHA : 0 ) );
	glutCreateWindow( "Genesaver" );
	//glutFullScreen();
	
	if ( settings.lizard ) world = new Jungle();
	else world = new World();
	load();

	glutSetCursor( GLUT_CURSOR_NONE );

	if ( settings.AA )
	{
		// Thanks to Adrian for making this work!
		// And thanks to Matt for helping me test the problem that
		// manifested only on his machine and noplace else.
		glEnable( GL_BLEND );
		glEnable( GL_POLYGON_SMOOTH );
		if ( settings.AAline) glEnable( GL_LINE_SMOOTH );
		glBlendFunc (GL_SRC_ALPHA, GL_ONE);
		glHint( GL_POLYGON_SMOOTH_HINT, GL_DONT_CARE );
	}

	glShadeModel( settings.shade ? GL_SMOOTH : GL_FLAT );

	glutDisplayFunc( display );
	glutKeyboardFunc( kdone );
	glutPassiveMotionFunc( mdone );

	glutTimerFunc( 1, timer, 0 );
	glutMainLoop();
	return 0;
}

void display()
{
	if ( draw() )
	{
		glFlush();
		glutSwapBuffers();
	}
}

void timer( int val )
{
	extern World *world;
	world->Step();
	glutPostRedisplay();
	glutTimerFunc( 1, timer, 0 );
}

void kdone( unsigned char c, int, int )
{
	extern World *world;

	switch ( c )
	{
	case 'c':
	case 'C':
		world->CamChange();
		if ( settings.camera == C_GRAPH )
		{
			glFlush();
			glutSwapBuffers();
		}
		break;
	case 'v':
		world->ViewChange();
		break;
	case '+':
	case '=':
		if ( settings.zoom < 1000 ) settings.zoom += 5;
		break;
	case '_':
	case '-':
		if ( settings.zoom > 10 ) settings.zoom -= 5;
		break;
	default:
		exit( save() );
	}
}

void mdone( int x, int y )
{
	if ( !mstart )
	{
		mx = x;
		my = y;
		mstart = true;
		return;
	}

	// Exit if the mouse moves more than 100 pixels.
	// if ( (x-mx)*(x-mx)+(y-my)*(y-my) < 10000 ) return;
	// marc: keep it up in a window if necessary..?

	// exit( save() );
	
	return;
}

#endif

#include <math.h>
#include <GL/glut.h>

#include "globals.h"
#include "DNA.h"
#include "Brain.h"
#include "Thing.h"
#include "Animal.h"
#include "Lizard.h"

Lizard::Lizard( DNA* dna )
:Animal( dna )
{
}

Lizard::~Lizard(void)
{
}

void Lizard::Render()
{
	glBegin( GL_POLYGON );
		//Head
		SetHue( diet * 5.0 / 6.0 );
		glVertex2f( x + A_R * cos( angle + 1.0 ), y + A_R * sin( angle + 1.0 ) );
		glVertex2f( x + A_R * cos( angle - 1.0 ), y + A_R * sin( angle - 1.0 ) );

		//Tail
		if ( settings.shade )
		{
			if ( fighting && mating ) SetColor( Cyan ); //birth
			else if ( fighting ) SetColor( Magenta ); //overcrowding
			else if ( mating ) SetColor( Yellow ); //just married
			else SetColor( color );
		}
		glVertex2f( x - A_R * cos( angle ), y - A_R * sin( angle ) );
	glEnd();

	if ( tagged && settings.wake )
	{
		glBegin( GL_LINES );
			glColor3f( 1, 1, 1 );
			glVertex2f( x-A_R*1.5*cos(angle) + A_R*0.5, 
				y-A_R*1.5*sin(angle) );
			if ( settings.shade ) glColor3f( 0, 0, 0 );
			glVertex2f( x-A_R*2.0*step*cos(angle)-A_R*1.5*cos(angle) + A_R*0.5, 
				y-A_R*2.0*step*sin(angle)-A_R*1.5*sin(angle) );
			glColor3f( 1, 1, 1 );
			glVertex2f( x-A_R*1.5*cos(angle) - A_R*0.5, 
				y-A_R*1.5*sin(angle) );
			if ( settings.shade ) glColor3f( 0, 0, 0 );
			glVertex2f( x-A_R*2.0*step*cos(angle)-A_R*1.5*cos(angle) - A_R*0.5, 
				y-A_R*2.0*step*sin(angle)-A_R*1.5*sin(angle) );
			glColor3f( 1, 1, 1 );
			glVertex2f( x-A_R*1.5*cos(angle), 
				y-A_R*1.5*sin(angle) + A_R*0.5 );
			if ( settings.shade ) glColor3f( 0, 0, 0 );
			glVertex2f( x-A_R*2.0*step*cos(angle)-A_R*1.5*cos(angle), 
				y-A_R*2.0*step*sin(angle)-A_R*1.5*sin(angle) + A_R*0.5 );
			glColor3f( 1, 1, 1 );
			glVertex2f( x-A_R*1.5*cos(angle), 
				y-A_R*1.5*sin(angle) - A_R*0.5 );
			if ( settings.shade ) glColor3f( 0, 0, 0 );
			glVertex2f( x-A_R*2.0*step*cos(angle)-A_R*1.5*cos(angle), 
				y-A_R*2.0*step*sin(angle)-A_R*1.5*sin(angle) - A_R*0.5 );
		glEnd();
	}
}

void Lizard::See( float d2, Color c, float d, float a )
{
	float f = 1.0 - ( d2 - A_R * A_R ) / ( A_V * A_V - A_R * A_R );
	float da = a - angle;
	if ( cos( da ) <= 0 ) return; //behind you
	int i;

	//Send input to appropriate neuron.
	// input[0] = plant
	// input[2] = same
	// input[4] = prey
	// input[6] = predator

	float dd = d - diet;

	if ( c == P_C ) i = 0;
	else if ( dd < -0.16 )
	{
		i = 4;
		f *= -dd;
	}
	else if ( dd > 0.16 )
	{
		i = 6;
		f *= dd;
	}
	else i = 2;

	if ( sin( da ) <= sin( A_A ) && sin( da ) >= sin( -A_L ) )//left
	{
		brain.input[i].axon += f;
	}
	if ( sin( da ) < sin( A_L ) && sin( da ) > sin( -A_A ) )//right
	{
		brain.input[i+1].axon += f;
	}
}

void Lizard::Step()
{
	// Things get kind of sludgy in this ruleset, so decrease the amount
	// of energy needed to move, and increase the amount needed just to live
	Animal::Step();
	float p = step < 0 ? A_B : 1.0;
	energy += step * A_M * A_S * p * 0.25;
	energy -= 3 * A_Z ;
}

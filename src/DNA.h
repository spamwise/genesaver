// Genesaver: copyright 2003 Sam Stafford.

#include <stdio.h>

struct IChromo
{
	char ax0[4];
	char wgt[4][4];
	char ax1[4];
	char colr;
	char diet;
};

struct HChromo
{
	char iden[12];
	char iwgt[12];
	char owgt[4][24];
	char colr[4];
	char diet[4];
};

class DNA
{
public:
	DNA( void ); //empty
	DNA( DNA* ); //clone
	DNA( DNA*, DNA* ); //crossover
	~DNA(void);

	void Dump( FILE* );
	char* Load( char* );
	char* LoadChar( char*, char* );
	void Mutate();
	void PutChar( FILE*, char );
	void Randomize();
	char Squish( char, char ); 

	HChromo hchr;
	IChromo ichr[6];
};

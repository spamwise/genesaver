// Genesaver: copyright 2003 Sam Stafford.

#include <GL/glut.h>

#include "globals.h"
#include "Thing.h"
#include "Plant.h"

Plant::Plant(void)
{
	prev = NULL;
	next = NULL;
	energy = P_E;
}

Plant::~Plant(void)
{
}

void Plant::Render()
{
	SetColor( P_C );
	glBegin( GL_POLYGON );
		glVertex2f( x + P_R, y + P_R * 0.5 );
		glVertex2f( x + P_R, y - P_R * 0.5 );
		glVertex2f( x + P_R * 0.5, y - P_R );
		glVertex2f( x - P_R * 0.5, y - P_R );
		glVertex2f( x - P_R, y - P_R * 0.5 );
		glVertex2f( x - P_R, y + P_R * 0.5 );
		glVertex2f( x - P_R * 0.5, y + P_R );
		glVertex2f( x + P_R * 0.5, y + P_R );
	glEnd();
}

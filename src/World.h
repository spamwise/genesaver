// Genesaver: copyright 2003 Sam Stafford.

class Thing;
class Plant;
class Animal;

struct GraphNode
{
	float r, g, b;
	float rc, gc, bc;
	float hpop;
	GraphNode *next;
};

class World
{
public:
	World(void);
	~World(void);

	void CamChange();
	virtual void CheckCollisions( Animal* );
	void Clone( Animal* );
	void Collide( Animal*, Plant* );
	virtual void Collide( Animal*, Animal* );
	virtual bool Conflict( Animal* );
	virtual void InsertAnimal( DNA* );
	bool Kill( Animal*, Animal* );
	void Load( char* c );
	void Mate( Animal*, Animal* );
	virtual void Remove( Animal* );
	void Render();
	virtual bool RenderGraph();
	void RenderTagged();
	void Save( FILE* f );
	void SpawnAnimal();
	void SpawnPlant();
	void Step();
	virtual void UpdateGraph();
	void ViewChange( Animal* a = NULL );
	
	int rgcount;

protected:
	void RenderGrid(); //draw grid

	Plant* plants;
	Animal* animals;
	Animal* tagged;

	GraphNode *graphhead, *graphtail;
	int graphcount;

	double r, rc, g, gc, b, bc, p;

	float ptimer; //plant timer
};

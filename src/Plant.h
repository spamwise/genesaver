// Genesaver: copyright 2003 Sam Stafford.

class Plant :
	public Thing
{
public:
	Plant(void);
	~Plant(void);

	void Render();

	Plant* prev;
	Plant* next;
};

class DNA;
class Animal;

struct GraphJNode
{
	float c, cd;
	float h, hd;
	float p;
	float hpop;
	GraphJNode *next;
	float hue;
};

class Jungle :
	public World
{
public:
	Jungle(void);

	void Collide( Animal*, Animal* );
	bool Conflict( Animal* );
	void InsertAnimal( DNA* );

	void Remove( Animal* );
	bool RenderGraph();
	void UpdateGraph();

	GraphJNode *gjhead, *gjtail;
	float c, cd, h, hd;
};

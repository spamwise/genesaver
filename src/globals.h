// Genesaver: copyright 2003 Sam Stafford.

//Enums, constants, and the like.
enum Color { Black, Grey, Green, Red, Blue, Yellow, Cyan, Magenta };

//settings that can be configured by the user
struct Settings
{
	bool AA;
	bool AAline;
	bool shade;
	bool save;
	short mutate;
	short plants;  // per 1000 ticks
	short cscale;  // percent of default size
	short cspeed;  // percent of default speed
	short cvision; // percent of default vision
	short camera;
	int gticks;    // timesteps per screen
	short gwidth;  // data points per screen
	short zoom;
	bool wake;
	bool grid;
	bool drawvis;
	int trail;
	bool lizard;
};

//global state variables that are not directly saved or user modified
struct FSettings
{
	float gtime;
	float gstep;
	float ps;
	float pr;
	float ar;
	float am;
	float as;
	float at;
	float av;
	float ta;

	float whratio; //screen width/height ratio

	short ac;

	float alpha;
	bool demo;
};

extern Settings settings;
extern FSettings fsettings;

#ifndef NULL
#define NULL 0		//null
#endif

#define P_E 500.0  	//Plant energy
#define P_R fsettings.pr 	//Plant radius
#define P_C Grey 	//Plant color
#define P_S	fsettings.ps
	//Plant spawning ( number of plants to spawn per step )

#define A_E 300.0  	//Animal energy
#define A_R fsettings.ar	//Animal radius
#define A_M fsettings.am	//Animal metabolism ( A_M * dist = energy used )
#define A_Z 0.05 	//Resting metabolism ( energy used per step )
#define A_D 0.4		//Animal digestion ( A_D * input = energy gained )
#define A_B	-1.5	//Animal backup ( factor of extra energy burned )
#define A_S	fsettings.as //Animal speed ( length of max step )
#define A_T fsettings.at //Animal turning ( max turn step, radians )
#define A_C	fsettings.ac //Number of steps an animal needs to chew
#define A_V fsettings.av	//Visual distance
#define A_A (float)1		//Wide angle of view
#define A_L (float)0.2		//"Lookat" angle of view

#define D_M settings.mutate	//Number of times to mutate DNA on a split

#define N_R 0.015	//Neuron radius when rendering brain
#define N_L 0.03	//Neuron label "radius"

#define C_WORLD 1	//"World" camera setting
#define C_CHASE 2	//"Chase" camera setting
#define C_GRAPH 3	//"Graph" camera setting

#define G_TIME fsettings.gtime
					//Ticks per graph point
#define G_STEP fsettings.gstep
					//Width in screen coords tween points

#include "util.h"

#ifdef WIN32
#include <direct.h>
#include <windows.h>
#else
#include <unistd.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include "globals.h"
#include "util.h"
#include "DNA.h"
#include "World.h"

extern World* world;
extern Settings settings;
extern FSettings fsettings;

//Globals.

World* world;
Settings settings;
FSettings fsettings;

//General utility functions.

bool draw()
{
	bool drew = true;
	switch( settings.camera )
	{
	case C_CHASE: world->RenderTagged(); break;
	case C_WORLD: world->Render(); break;
	case C_GRAPH: drew = world->RenderGraph(); break;
	}
	return drew;
}

void load()
{
	FILE* f = fopen( "Genes.txt", "r" );

	if ( !f ) return;

	fseek( f, 0, SEEK_END );
	int size = ftell( f );
	fseek( f, 0, SEEK_SET );

	if ( !size ) return;

	char* dfile = new char[size];
	for ( int a = 0 ; a < size ; a++ ) dfile[a] = fgetc( f );

	fclose( f );

	world->Load( dfile );
}

int save()
{
	if ( !settings.save || fsettings.demo ) return 0;

	FILE* f = fopen( "Genes.tmp", "w" );
	if ( !f ) return 1;
	world->Save( f );
	int e = fclose( f );
	if ( e ) return e;

	unlink( "Genes.txt" );

	return rename( "Genes.tmp", "Genes.txt" );
}

float RandFloat()
{ 
	 return rand() / ( 1.0f * RAND_MAX );
}

int RandInt( int foo )
{
	return rand() % ( foo + 1 );
}

void SetColor( Color c )
{
	float alpha = fsettings.alpha;
	switch ( c )
	{
	case Black:
		glColor4f( 0.0, 0.0, 0.0, alpha );
		break;
	case Blue:
		glColor4f( 0.0, 0.0, 1.0, alpha );
		break;
	case Cyan:
		glColor4f( 0.0, 1.0, 1.0, alpha );
		break;
	case Green:
		glColor4f( 0.0, 1.0, 0.0, alpha );
		break;
	case Grey:
		glColor4f( 0.5, 0.5, 0.5, alpha );
		break;
	case Magenta:
		glColor4f( 1.0, 0.0, 1.0, alpha );
		break;
	case Red:
		glColor4f( 1.0, 0.0, 0.0, alpha );
		break;
	case Yellow:
		glColor4f( 1.0, 1.0, 0.0, alpha );
		break;
	}
}

void SetColor21( Color c, double f ) //set color for graph
{
	double r, g, b, inc, dec;
	inc = dec = 0.0;
	f *= 1.2;
	if ( f < 0.0 ) f = 0.0;
	else if ( f > 1.0 ) f = 1.0;
	if ( f < 0.5 ) //darken primary
		dec = ( 1.0 - 2.0 * f ) * 0.64;
	else //brighten all others
		inc = ( 2.0 * f - 1.0 ) * 0.64;
	switch( c )
	{
	case Red:
		r = 1.0 - dec;
		g = b = inc;
		break;
	case Green:
		g = 1.0 - dec;
		r = b = inc;
		break;
	case Blue:
		b = 1.0 - dec;
		r = g = inc;
		break;
	}
	glColor3f( float( r ), float( g ), float( b ) );
}

void SetHue( double d )
{
	if ( d > 1.0 ) d = 1.0;
	else if ( d < 0.0 ) d = 0.0;

	double r, g, b;
	r = g = b = 0.0;
	d += 1.0 / 6.0;
	if ( d > 1.0 ) d -= 1.0;
	if ( d <= 1.0 / 3.0 )
	{
		r = -3.0 * d + 1.0;
		b = 3.0 * d;
	}
	else if ( d <= 2.0 / 3.0 )
	{
		d -= 1.0 / 3.0;
		b = -3.0 * d + 1.0;
		g = 3.0 * d;
	}
	else
	{
		d -= 2.0 / 3.0;
		g = -3.0 * d + 1.0;
		r = 3.0 * d;
	}

	glColor4f
		( float( r ), float( g ), float( b ), fsettings.alpha );
}

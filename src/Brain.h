// Genesaver: copyright 2003 Sam Stafford.

class DNA;

struct Neuron 
{ 
	float axon;
	float thres;
	Neuron* dendrite[4];
	float weight[4];
	float x;
	bool active;
};

struct Lobe
{
	Neuron neur[2][4];
};

struct BigNeuron
{
	float axon;
	Neuron* dendrite[24];
	float weight[24];
};

class Brain
{
public:
	Brain( DNA*, float*, float* );
	~Brain(void);

	void CheckActive( Neuron* );
	void Clear( float );
	void Fire( Neuron* );
	void Fire( BigNeuron* );
	void Render();
	void RenderLights();
	void RenderSyn( float weight, Neuron* n1, float y1, float x2, float y2 );
	void RenderNeuron( float axon, float x, float y );
	void Think();

	Neuron input[14]; //5x2 for colors, 1 energy, 1 step, 2 feedback
	Lobe lobe[6];
	BigNeuron output[4];

	float *step, *turn;
	float feed1, feed2;

	Color color;
};

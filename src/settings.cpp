// Genesaver: copyright 2003 Sam Stafford.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#ifdef WIN32
	#include <direct.h>
	extern int Width, Height;
#endif

#include "globals.h"

#define LTG_ABOUT "\
# Genesaver - a living screensaver\n\
# from http://www.leadtogold.com\n\
# \n\
# Thanks to:\n\
# Matt: feedback and testing\n\
# Adrian: OpenGL noob help\n\
# Nora and Mandi: ecosystem help\n\
"

#define LTG_INSTR "\
# \n\
# When screensaver is running, push 'c' to switch camera mode.\n\
# When in \"chase-cam\" mode, push 'v' to switch viewed creature.\n\
# When in \"world\" mode, push numpad + and - to zoom in and out.\n\
# \n\
"

void calcfsettings()
{
	fsettings.gtime = float( settings.gticks ) / float( settings.gwidth );
	fsettings.gstep = float( 2.0 / settings.gwidth );
	fsettings.ps = float( settings.plants / 1000.0 );
	fsettings.pr = float( settings.cscale * 0.005 / 100.0 );
	fsettings.ar = float( settings.cscale * 0.01 / 100.0 );
	fsettings.am = float( 25.0 * 100.0 / settings.cscale );
	fsettings.as = float( settings.cscale * settings.cspeed * 0.00000075 );
	fsettings.at = float( settings.cspeed * 0.5 / 100.0 );
	fsettings.av = float( settings.cscale * 0.2 / 100.0 );
	fsettings.av *= float( settings.cvision / 100.0 );

	fsettings.ac = short( 2000 / settings.cspeed );
	if ( fsettings.ac < 1 ) fsettings.ac = 1;

	fsettings.ta = float( 1.0 / settings.trail );
}

void savesettings()
{
	if ( fsettings.demo ) return;

	FILE* f = fopen( "Genesaver.cfg", "w" );
	if ( !f ) return;

	char c;

	fprintf( f, LTG_ABOUT );
	fprintf( f, LTG_INSTR );

	fprintf( f, "#\n#\n# SETTINGS\n#\n" );

	fprintf( f, "#\n# Simulation settings\n#\n" );

	fprintf( f, "# Save genotypes on exit?\n" );
	c = settings.save ? 'T' : 'F';
	fprintf( f, "savegenes=%c\n", c );

	fprintf( f, "# Number of mutations per birth?\n" );
	fprintf( f, "mutations=%d\n", settings.mutate );

	fprintf( f, "# Number of plants to grow per 1000 steps?\n" );
	fprintf( f, "plantgrow=%d\n", settings.plants );

	fprintf( f, "# Creature size (percent of normal)?\n" );
	fprintf( f, "crtrscale=%d\n", settings.cscale );

	fprintf( f, "# Creature speed (percent of normal)?\n" );
	fprintf( f, "crtrspeed=%d\n", settings.cspeed );

	fprintf( f, "# Creature vision (percent of normal)?\n" );
	fprintf( f, "crtrsight=%d\n", settings.cvision );

	fprintf( f, "# Use weird alternate ecosystem?\n" );
	c = settings.lizard ? 'T' : 'F';
	fprintf( f, "weirdness=%c\n", c );

	fprintf( f, "#\n# Graphics settings\n#\n" );

	fprintf( f, "# Enable antialiasing and alpha blending?\n" );
	c = settings.AA ? 'T' : 'F' ;
	fprintf( f, "antialias=%c\n", c );

	fprintf( f, "# Antialias lines as well as polygons?\n" );
	c = settings.AAline ? 'T' : 'F';
	fprintf( f, "aalinetoo=%c\n", c );

	fprintf( f, "# Enable smooth shading?\n" );
	c = settings.shade ? 'T' : 'F';
	fprintf( f, "doshading=%c\n", c );

	fprintf( f, "#\n# View settings\n#\n" );

	fprintf( f, "# Default camera setting?  W for world, C for chase, G for graph\n" );
	if ( settings.camera == C_CHASE ) c = 'C';
	else if ( settings.camera == C_GRAPH ) c = 'G';
	else c = 'W';
	fprintf( f, "setcamera=%c\n", c );

	fprintf( f, "# Zoom percent in world view?\n" );
	fprintf( f, "zoomprcnt=%d\n", settings.zoom );

	fprintf( f, "# Draw speed lines behind viewed creature?\n" );
	c = settings.wake ? 'T' : 'F' ;
	fprintf( f, "speedline=%c\n", c );

	fprintf( f, "# Blur trail length in world view (1 to disable)\n" );
	fprintf( f, "blurtrail=%d\n", settings.trail );

	fprintf( f, "# Draw grid?\n" );
	c = settings.grid ? 'T' : 'F' ;
	fprintf( f, "gridlines=%c\n", c );

	fprintf( f, "# Limit chase-cam drawing to visual range?\n" );
	c = settings.drawvis ? 'T' : 'F' ;
	fprintf( f, "drawvisbl=%c\n", c );

	fprintf( f, "# Number of timesteps to cover per graph screen?\n" );
	fprintf( f, "graphtime=%d\n", settings.gticks );

	fprintf( f, "# Number of points to plot per graph screen?\n" );
	fprintf( f, "graphpnts=%d\n", settings.gwidth );

	fclose( f );
}

void loadsettings()
{
	settings.AA = true;
	settings.AAline = true;
	settings.shade = true;
	settings.save = true;
	settings.mutate = 9;
	settings.plants = 200;
	settings.cscale =  50;
	settings.cspeed = 100;
	settings.cvision = 100;
	settings.camera = C_WORLD;
	settings.gticks = 20000;
	settings.gwidth = 200;
	settings.wake = false;
	settings.grid = false;
	settings.drawvis = true;
	settings.lizard = false;
	settings.trail = 1;

	fsettings.whratio = 125.0;
	fsettings.alpha = 1.0;
	fsettings.demo = false;

#ifdef WIN32
	if ( Width > 300 && Width < 3000 )
	    fsettings.whratio = float( Width ) * 100 / float( Height );

	if( Width < 400 )
	    fsettings.demo = true;
#endif

	settings.zoom = short( ceil( fsettings.whratio ) );

	FILE* f = fopen( "Genesaver.cfg", "r" );
	if ( !f )
	{
	    if( !fsettings.demo )
	    {
		calcfsettings();
		savesettings();
	    }
	    return;
	}
	char c[64];
	while ( fgets( c, 64, f ) )
	{
		if ( !memcmp( c, "antialias=T", 11 ) )
			settings.AA = true;
		else if ( !memcmp( c, "antialias=F", 11 ) )
			settings.AA = false;
		else if ( !memcmp( c, "aalinetoo=T", 11 ) )
			settings.AAline = true;
		else if ( !memcmp( c, "aalinetoo=F", 11 ) )
			settings.AAline = false;
		else if ( !memcmp( c, "doshading=T", 11 ) )
			settings.shade = true;
		else if ( !memcmp( c, "doshading=F", 11 ) )
			settings.shade = false;
		else if ( !memcmp( c, "savegenes=T", 11 ) )
			settings.save = true;
		else if ( !memcmp( c, "savegenes=F", 11 ) )
			settings.save = false;
		else if ( !memcmp( c, "setcamera=W", 11 ) )
			settings.camera = C_WORLD;
		else if ( !memcmp( c, "setcamera=C", 11 ) )
			settings.camera = C_CHASE;
		else if ( !memcmp( c, "setcamera=G", 11 ) )
			settings.camera = C_GRAPH;
		else if ( !memcmp( c, "speedline=T", 11 ) )
			settings.wake = true;
		else if ( !memcmp( c, "speedline=F", 11 ) )
			settings.wake = false;
		else if ( !memcmp( c, "gridlines=T", 11 ) )
			settings.grid = true;
		else if ( !memcmp( c, "gridlines=F", 11 ) )
			settings.grid = false;
		else if ( !memcmp( c, "drawvisbl=T", 11 ) )
			settings.drawvis = true;
		else if ( !memcmp( c, "drawvisbl=F", 11 ) )
			settings.drawvis = false;
		else if ( !memcmp( c, "weirdness=T", 11 ) )
			settings.lizard = true;
		else if ( !memcmp( c, "weirdness=F", 11 ) )
			settings.lizard = false;
		else if ( !memcmp( c, "mutations=", 10 ) )
			settings.mutate = atoi( c+10 );
		else if ( !memcmp( c, "plantgrow=", 10 ) )
			settings.plants = atoi( c+10 );
		else if ( !memcmp( c, "graphtime=", 10 ) )
			settings.gticks = atoi( c+10 );
		else if ( !memcmp( c, "graphpnts=", 10 ) )
			settings.gwidth = atoi( c+10 );
		else if ( !memcmp( c, "zoomprcnt=", 10 ) )
			settings.zoom = atoi( c+10 );
		else if ( !memcmp( c, "crtrscale=", 10 ) )
			settings.cscale = atoi( c+10 );
		else if ( !memcmp( c, "crtrspeed=", 10 ) )
			settings.cspeed = atoi( c+10 );
		else if ( !memcmp( c, "crtrsight=", 10 ) )
			settings.cvision = atoi( c+10 );
		else if ( !memcmp( c, "blurtrail=", 10 ) )
			settings.trail = atoi( c+10 );
	}

	fclose( f );

	//Sanity checks
	if ( settings.mutate < 0 ) settings.mutate = 9;
	if ( settings.plants < 0 ) settings.plants = 200;
	if ( settings.gwidth < 1 ) settings.gwidth = 200;
	if ( settings.gticks < settings.gwidth ) 
		settings.gticks = settings.gwidth ;
	if ( settings.zoom < 50 || settings.zoom > 10000 ) settings.zoom = 100;
	if ( settings.zoom < fsettings.whratio ) 
	     settings.zoom = short( ceil( fsettings.whratio ) );
	if ( settings.cscale < 1 || settings.cscale > 1000 ) settings.cscale = 100;
	if ( settings.cspeed < 1 ) settings.cspeed = 100;
	if ( settings.cspeed > 4000 ) settings.cspeed = 4000;
	if ( settings.trail < 1 ) settings.trail = 1;

	//Pre-process settings as needed
	calcfsettings();
}


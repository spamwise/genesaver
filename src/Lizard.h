class DNA;

class Lizard :
	public Animal
{
public:
	Lizard( DNA* );
	~Lizard(void);

	void Render();
	void See( float d2, Color c, float diet, float angle );
	void Step();
};

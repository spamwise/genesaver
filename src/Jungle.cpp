#include <GL/glut.h>
#define  _USE_MATH_DEFINES
#include <math.h>
#include <stdio.h>

#include "globals.h"
#include "Brain.h"
#include "Thing.h"
#include "Plant.h"
#include "Animal.h"
#include "Lizard.h"
#include "World.h"
#include "Jungle.h"

Jungle::Jungle(void)
: World()
{
	gjhead = gjtail = NULL;
	c = cd = h = hd = 0.0;
}

void Jungle::Collide( Animal* c, Animal* h )
{
	if ( c->diet >= h->diet && c->diet <= h->diet + 0.16  )
	{
		Mate( c, h );
		return;
	}
	else if ( c->diet > h->diet + 0.16 )
	{
		if ( c->achewing ) return;

		c->achewing = A_C;

		c->energy += float( A_E * A_D * ( c->diet - h->diet / 4.0 ) );
		h->energy -= float( A_E * ( c->diet - h->diet / 4.0 )  );

		if ( h->tagged && h->energy < 0.0 ) ViewChange( c );

		return;
	}
}

bool Jungle::Conflict( Animal* a )
{
	return ( ( rc + gc + bc ) * 3 > r + g + b  ) &&
		a->diet > hd * 1.05 / h;
}

void Jungle::InsertAnimal( DNA* dna )
{
	if ( !animals )
	{
		animals = new Lizard( dna );
	}
	else
	{
		animals->prev = new Lizard( dna );
		animals->prev->next = animals;
		animals = animals->prev;
	}
	switch( animals->color )
	{
	case Red: r++; rc += animals->diet; break;
	case Green: g++; gc += animals->diet; break;
	case Blue: b++; bc += animals->diet; break;
	}

	if ( animals->diet > 0.5 )
	{
		c++;
		cd += animals->diet;
	}
	else
	{
		h++;
		hd += animals->diet;
	}
}

void Jungle::Remove( Animal* a )
{
	if ( a->diet > 0.5 )
	{
		c--;
		cd -= a->diet;
	}
	else
	{
		h--;
		hd -= a->diet;
	}
	World::Remove( a );
}

bool Jungle::RenderGraph()
{
	if ( rgcount != 1 ) return false;

	GraphJNode* foo;
	float gx = -1.0;
	float cf = 5.0 / 6.0;

	glClear( GL_COLOR_BUFFER_BIT );

	glBegin( GL_LINE_STRIP );
	SetColor( Red );
	for ( foo = gjhead; foo; foo = foo->next )
	{
		if ( settings.shade && foo->c ) SetHue( cf * foo->cd / foo->c );
		glVertex2f( gx, foo->c / foo->hpop - 1.0 );
		gx += G_STEP;
	}
	glEnd();

	gx = -1.0;
	glBegin( GL_LINE_STRIP );
	SetColor( Blue );
	for ( foo = gjhead; foo; foo = foo->next )
	{
		if ( settings.shade && foo->h ) SetHue( cf * foo->hd / foo->h );
		glVertex2f( gx, foo->h / foo->hpop - 1.0 );
		gx += G_STEP;
	}
	glEnd();

	gx = -1.0;
	glBegin( GL_LINE_STRIP );
	if ( !settings.shade ) SetColor( P_C );
	for ( foo = gjhead; foo; foo = foo->next )
	{
		if ( settings.shade ) SetHue( foo->hue );
		glVertex2f( gx, foo->p / foo->hpop - 1.0 );
		gx += G_STEP;
	}
	glEnd();

	return true;
}

void Jungle::UpdateGraph()
{
	float prevhue = 1.0 / 6.0;

	if ( rgcount )
	{
		rgcount++;
		if ( rgcount > G_TIME ) rgcount = 0;
		return;
	}
	rgcount++;

	if ( !gjhead )
	{
		gjtail = new GraphJNode();
		gjhead = gjtail;
	}
	else
	{
		GraphJNode* gn = new GraphJNode();
		gjtail->next = gn;
		prevhue = gjtail->hue;
		gjtail = gn;
	}
	gjtail->next = NULL;

	gjtail->c = this->c;
	gjtail->h = this->h;
	gjtail->cd = this->cd;
	gjtail->hd = this->hd;
	gjtail->p = this->p;
	gjtail->hue = prevhue + G_STEP / 2.0;
	if ( gjtail->hue > 1.0 ) gjtail->hue = 0.0;

	gjtail->hpop = ( c + h + p ) / 2.0;
	graphcount++;
	while ( graphcount > 2.0 / G_STEP + 1.0 )
	{
		GraphJNode* hn = gjhead;
		gjhead = gjhead->next;
		delete hn;
		graphcount--;
	}
}

// Genesaver: copyright 2003 Sam Stafford.

class Thing
{
public:
	Thing(void);
	virtual ~Thing(void);

	virtual void Render() = 0;
	void RenderBy( float ax, float ay );

	float XNearest( float x );
	float YNearest( float y );

	float x, y;
	float energy;
};
